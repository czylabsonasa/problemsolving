from sys import stdin; input=stdin.readline

T=int(input())
while T>0:
  T-=1
  s=input().strip()
  p=s[0]
  l=1
  for v in s[1:]:
    if v!=p:
      l+=1
      p=v
      if l==3:break
    else:
      l=1
  if l==3:
    print('Good')
  else:
    print('Bad')
