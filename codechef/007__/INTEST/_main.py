from sys import stdin; input=stdin.readline
n,k=[int(v) for v in input().split()]
ans=0
for _ in range(n):
  x=int(input())
  ans+=(x%k==0)
print(ans)