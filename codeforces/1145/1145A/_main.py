n=int(input())
arr=list(map(int,input().split()))
mx=1
def solve(a,b):
  global mx
  if a==b: return True
  m=(a+b)//2
  ll=solve(a,m)
  rr=solve(m+1,b)
  if ll==True and rr==True and arr[m]<=arr[m+1]:
    mx=max(mx,b-a+1)
    return True
  return False
solve(0,n-1)
print(mx)
  
    