from sys import stdin; input=stdin.readline
from math import sqrt

H,L=map(float,input().split())
print(0.5*(L*L+H*H)/H-H)
