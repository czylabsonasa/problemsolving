n=int(input())
log=input().strip()
r1=[0]*10
r2=[0]*10
for c in log:
  if c=='L':
    i=r1.index(0)
    r1[i]=r2[9-i]=1
    continue
  if c=='R':
    i=r2.index(0)
    r2[i]=r1[9-i]=1
    continue
  i=int(c)
  r1[i]=r2[9-i]=0
print("".join([str(c) for c in r1]))  
    