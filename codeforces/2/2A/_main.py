n=int(input())
dd={}
arr=[]
for _ in range(n):
  a,pa=input().split()
  pa=int(pa)
  if dd.get(a)==None: dd[a]=0
  dd[a]+=pa
  arr.append((a,pa))
mx=max(dd.values())
dd2={k:0 for k in dd.keys() if dd[k]==mx}
w=''
for i in range(n):
  a,pa=arr[i]
  if dd2.get(a)==None: continue
  dd2[a]+=pa
  if dd2[a]>=mx: w=a;break
print(w)
