from sys import stdin; input=stdin.readline
volt=[0]*(10**6+8)

n=int(input())
INF=n+9
first=[INF]*INF
nxt=[0]*INF
arr=[0]*INF
frek=[0]*INF

def merge(i,j):
  i,j=first[i],first[j]
  ans=0
  while i!=INF or j!=INF:
    if(i<j):
      while i<j: i=nxt[i]
      ans+=1
    else:
      while j<i: j=nxt[j]
      ans+=1
  return ans

i=1
ndiff=0
for v in input().split():
  v=int(v)
  if volt[v]==0:
    ndiff+=1
    volt[v]=ndiff
  vv=volt[v]
  arr[i]=vv; i+=1
  frek[vv]+=1

for i in range(n,0,-1):
  v=arr[i]
  nxt[i]=first[v]
  first[v]=i

#print(nxt[:ndiff+1])
ans=max(frek[1:ndiff+1])
for i in range(1,ndiff):
  for j in range(i+1,ndiff+1):
    
    if frek[i]+frek[j]<=ans: continue
    ans=max(ans,merge(i,j))      

print(ans)
