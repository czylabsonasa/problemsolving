r,c,Boss=input().split()
r,c=int(r),int(c)
tab=[]
for _ in range(r):
  tab.append(list(input()))
dd={}
for i in range(r):
  for j in range(c):
    t=tab[i][j]
    if t=='.' or t==Boss or dd.get(t)!=None: continue
    if i-1>=0 and tab[i-1][j]==Boss:
      dd[t]=True; continue
    if i+1<r and tab[i+1][j]==Boss:
      dd[t]=True; continue
    if j-1>=0 and tab[i][j-1]==Boss:
      dd[t]=True; continue
    if j+1<c and tab[i][j+1]==Boss:
      dd[t]=True; continue
print(len(dd.keys()))    
