from sys import stdin; input=stdin.readline
n,k=map(int,input().split())
p='('
mx=0
akt=0
for v in sorted(input().strip())+[')']:
	if v==p: 
		akt+=1
	else:
		mx=max(akt,mx)
		if mx>k: break
		akt=1
		p=v		

if mx>k:
	print('NO')
else:
	print('YES')