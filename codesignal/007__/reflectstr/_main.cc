#include <bits/stdc++.h>
using namespace std;
std::string reflectString(string s){
    for(auto& a : s)
        a = 'z' - (a - 'a');
    return s;
}

int main(){
  while(1){
    string s; cin>>s; cout<<reflectString(s)<<"\n";
  }
  return 0;
}
