function starRotation(matrix, width, center, t)
  lay=div(width-1,2)
  t=t%8; t=(8-t)%8
  (lay==0 || t==0)&&(return matrix)
  center = center .+ [1,1]  
  function mklay(s,h)
    local ret=[]
    irany=[0,-1]
    for _ in 1:4
      ret=vcat(ret,[s+div(h-1,2).*irany,s+(h-1).*irany])
      s=last(ret)
      irany=[-irany[2],irany[1]]
    end
    ret
  end
  ret=deepcopy(matrix)
  lays=[mklay(center .+ [-i,i],2*i+1) for i in 1:lay]
  for i in 1:lay
    tli=circshift(lays[i],t)
    #println(lays[i])
    #println(tli)
    for j in 1:8
      a,b=lays[i][j]
      ta,tb=tli[j]
      ret[a][b]=matrix[ta][tb]
    end
  end
  ret 
end    