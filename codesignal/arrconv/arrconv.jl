function arrconv(a)
  o,p=+,*
  while length(a)>1 
    a=o.(a[1:2:end],a[2:2:end]) 
    o,p=p,o
  end
  sum(a)
end
