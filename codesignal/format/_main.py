def newStyleFormatting(s):
  out=[]
  prev=0
  for c in list(s):
    if c=='%':
      if prev==1:
        prev=0
      else:
        out.append(c)
        prev=1
    else:
      if prev==1:
        if c.isalpha(): 
          out.pop()
          out.extend(['{','}'])
        else:
          out.append(c)
        prev=0
      else:
        out.append(c)
  return ''.join(out)

ss=[
"Much %%s we have here!",
"%d%d%%-growth in products is expected quite soon",
"We expect the %f%% growth this week",
'nothing to insert or modify...... :-)',
"New style formatting (like %s) is waay cooler than old-style (i.e. %s)",
"%%%%x",
"%%%%%d"
]

for s in ss:
  print(newStyleFormatting(s))
