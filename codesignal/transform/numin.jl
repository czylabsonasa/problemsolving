function numberMinimization2(n,d)
  b=fill(Int8(0),1<<20)
  h=0
  A=1
  a=fill(0,1<<20)
  a[1]=n
  s=[]
  function p(i)
    if i==h
      S=parse(Int,join(s))
      b[S]=1
      (S%d==0)&&(A+=1;a[A]=S÷d)
      return
    end
    for j=i:h
      s[[i,j]]=s[[j,i]]
      p(i+1)
      s[[i,j]]=s[[j,i]]
    end
  end
  while A>0
    n=a[A];A-=1
    s=collect(string(n))
    h=length(s)
    b[n]==0&&p(1)
  end
  findfirst(isone,b)
end


function numberMinimization3(n,d)
  b=fill(Int8(0),1<<20)
  A=1
  a=fill(0,1<<20)
  a[1]=n
  s=[]
  function p(i)
    if i<1
      S=parse(Int,join(s))
      b[S]=1
      (S%d==0)&&(A+=1;a[A]=S÷d)
      return
    end
    for j=i:-1:1
      s[[i,j]]=s[[j,i]]
      p(i-1)
      s[[i,j]]=s[[j,i]]
    end
  end
  while A>0
    n=a[A];A-=1
    s=collect(string(n))
    b[n]==0&&p(length(s))
  end
  findfirst(isone,b)
end

using Combinatorics #nincsen a codesignalon
function numberMinimization4(n,d)
  b=fill(Int8(0),1<<20)
  a=[n]
  while length(a)>0
    s=sort(collect(string(pop!(a))))
    for v=permutations(s)
      V=parse(Int,join(v))
      #println(V)
      b[V]=1
      (V%d==0)&&(b[V÷d]==0)&&(push!(a,V÷d))
    end
  end
  findfirst(isone,b)
end


function numberMinimization(n,d)
  m=10^(1+Int(floor(log(n)/log(10))))-1
  s=fill(false,m)
  c=digits.(1:m)
  a=[parse(Int,join(sort(c[v]))) for v=1:m]
  b=[[] for i=1:m]
  for i=1:m
    push!(b[a[i]],i)
  end
  c=length.(c)
  function h(y)
    s[y]&&return
    cc=[v for v=b[a[y]] if c[v]≤c[y]]
    for v=cc
      s[v]=true
      (v%d==0)&&h(v÷d)
    end
  end
  h(n)
  findfirst(x->x==true,s)
end


# function numberMinimization(n,d)
#   m=10^(1+Int(floor(log(n)/log(10))))-1
#   q=fill(0,m)
#   s=fill(0,m)
#   c=digits.(1:m)

#   a=[parse(Int,join(sort(c[v]))) for v=1:m]
#   b=[[] for i=1:m]
#   for i=1:m
#     push!(b[a[i]],i)
#   end
#   c=length.(c)
# #println(b)
#   i=1
#   j=1
#   f(y,p)=(s[y]==0)&&(c[y]<=c[p])&&(s[y]=1;true)&&(y%d==0)&&(q[j]=y;j+=1)
#   f.(b[a[n]],n)
#   #println(q[1:j-1])
#   while i<j
#     x=q[i]÷d
#     i+=1
#     #f(x÷d,x);
#     f.(b[a[x]],x)
#   end
#   findfirst(isone,s)
# end

# function numberMinimization0(n,d)
#   h,l=digits,length
#   m=10^(1+Int(floor(log(n)/log(10))))-1
#   q=fill(n,m)
#   s=fill(0,m)
#   a=[parse(Int,join(sort(h(v)))) for v=1:m]
#   b=[[] for i=1:m]
#   for i=1:m
#     push!(b[a[i]],i)
#   end
# #println(b)
#   i=1
#   j=1
#   f(y,p)=(s[y]==0)&&(l(h(y))<=l(h(p)))&&(s[y]=1;true)&&(y%d==0)&&(q[j]=y;j+=1)
#   f.(b[a[n]],n)
#   #println(q[1:j-1])
#   while i<j
#     x=q[i]÷d
#     i+=1
#     #f(x÷d,x);
#     f.(b[a[x]],x)
#   end
#   findfirst(isone,s)
# end




# function numberMinimization(n,d)
#   h,l=digits,length
#   m=10^(1+Int(floor(log(n)/log(10))))-1
#   q=fill(n,m)
#   s=Set()
#   a=[parse(Int,join(sort(h(v)))) for v=1:m]
#   b=[[] for i=1:m]
#   for i=1:m
#     push!(b[a[i]],i)
#   end
# #println(b)
#   i=1
#   j=1
#   f(y,p)=!(y in s)&&(l(h(y))<=l(h(p)))&&(push!(s,y);true)&&(y%d==0)&&(q[j]=y;j+=1)
#   f.(b[a[n]],n)
#   println(q[1:j-1])
#   while i<j
#     x=q[i]÷d
#     i+=1
#     #f(x÷d,x);
#     f.(b[a[x]],x)
#   end
#   min(s...)
# end

# function numberMinimization(n,d)
#   h,l=digits,length
#   m=10^(1+Int(floor(log(n)/log(10))))-1
#   q=fill(n,m)
#   s=Set(n)
#   a=[parse(Int,join(sort(h(v)))) for v=1:m]
#   b=[[] for i=1:m]
#   for i=1:m
#     push!(b[a[i]],i)
#   end
#   i=1
#   j=2
#   f(y,p)=!(y in s)&&(l(h(y))<=l(h(p)))&&(push!(s,y);q[j]=y;j+=1)
#   while i<j
#     x=q[i]
#     i+=1
#     (x%d==0)&&f(x÷d,x);
#     f.(b[a[x]],x)
#   end
#   min(s...)
# end