function decode(str)
  ret = Array{String,1}()
  per,num=false,-1
  n,i=length(str),1
  while i<=n
    #println(i," ",num)
    c=str[i]
    if per==true
      if isdigit(c)
        (num<0)&&(num=0)
        num=num*10+c-'0'
        i+=1
      else
        per=false
        if num>=0
          ig=min(n,i+num-1)
          push!(ret,str[i:ig])
          i=ig+1
          num=-1
        else
          push!(ret,"\\")
        end
      end
    else
      if c=='\\'
        per=true
      else
        push!(ret,string(c))
      end
      i+=1
    end
  end
  
  ret
end

function decode2(str)
  ret = Array{String,1}()
  n=length(str)
  i=1
  while true
    (i>n) && break
    f=findnext(r"\\[0-9]+",str,i)
    (f==nothing) && break
    while i<f.start
      push!(ret,string(str[i]))
      i+=1
    end
    g=parse(Int,str[f.start+1:f.stop])
    ii=min(n,f.stop+g)
    push!(ret,str[f.stop+1:ii])
    i=ii+1
  end
  while i<=n
    push!(ret,string(str[i]))
    i+=1
  end
  ret  
end


function decode3(str)
  ret = Array{String,1}()
  n=length(str)
  i=1
  while true
    (i>n) && break
    f=findnext(r"\\[0-9]+",str,i)
    (f==nothing) && break
    ret=vcat(ret,map(string,collect(str[i:f.start-1])))
    ii=min(n,f.stop+parse(Int,str[f.start+1:f.stop]))
    push!(ret,str[f.stop+1:ii])
    i=ii+1
  end
  vcat(ret,map(string,collect(str[i:n])))
end


for str in [raw"abc\5defghi\2jkl", raw"\5ab\3cde","\\10a\\1bc"]
  println(decode3(str))
end

for str in [raw"abc\5defghi\2jkl", raw"\5ab\3cde","\\10a\\1bc"]
  println(decode2(str))
end

