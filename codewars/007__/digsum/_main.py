def comp(i,arr,narr):
  sn=sum(arr[:i])+arr[i]-1+(narr-1-i)*9
  n=int(''.join(str(d) for d in arr[:i])+str(arr[i]-1)+'9'*(narr-1-i))
  return sn,n

def solve(n):
  dn=[int(c) for c in str(n)]
  ldn=len(dn)
  sopt,opt=comp(0,dn,ldn)
  for i in range(1,ldn-1):
    if dn[i]>0: 
      so,o=comp(i,dn,ldn)
      if so>=sopt: opt,sopt=o,so
  if sum(dn)>=sopt: opt=n
  return opt

while True:
  print(solve(int(input())))
