def f(x,sx):
  if x==0: return ''
  ret=str(x)+' '+sx
  if x>1: ret+='s'
  return ret
  
def format_duration(s):
  ret='now'
  if s>0:
    M=60; H=60*M; D=24*H; Y=365*D
    m=h=d=y=0
    y,s=divmod(s,Y)
    d,s=divmod(s,D)
    h,s=divmod(s,H)
    m,s=divmod(s,M)
    arr=[x for x in [f(y,'year'),f(d,'day'),f(h,'hour'),f(m,'minute'),f(s,'second')] if len(x)>1]
    ret=', '.join(arr[:len(arr)-1])
    if len(ret)>0: ret=' and '.join([ret,arr[-1]])
    else: ret=arr[-1]
  return ret

while True:
  print(format_duration(int(input())))
