# ~ class DynamicConnectivity(object):
  # ~ def __init__(_s,N):
    # ~ _s.H=[i for i in range(N)]
    # ~ _s.W=[1]*N
  # ~ def find(_s,x):
    # ~ if _s.H[x]!=x:
      # ~ _s.H[x]=_s.find(_s.H[_s.H[x]])
    # ~ return _s.H[x] 
  # ~ def union(_s,x,y):
    # ~ x=_s.find(x); y=_s.find(y)
    # ~ if _s.W[y]>=_s.W[x]: _s.W[y]+=_s.W[x]; _s.H[x]=y; 
    # ~ else: _s.W[x]+=_s.W[y];_s.H[y]=x
  # ~ def connected(_s,x,y):
    # ~ x=_s.find(x); y=_s.find(y)
    # ~ return x==y

# ~ class DynamicConnectivity(object):
  # ~ def __init__(_s,N):
    # ~ _s.H=[i for i in range(N)]
  # ~ def find(_s,x):
    # ~ hx=_s.H[x]; hhx=_s.H[hx]
    # ~ while hx!=hhx:
      # ~ _s.H[hx]=_s.H[hhx]
      # ~ hx=hhx; hhx=_s.H[hx]
    # ~ _s.H[x]=hhx
    # ~ return hhx 
  # ~ def union(_s,x,y):
    # ~ _s.H[_s.find(x)]=_s.find(y); 
  # ~ def connected(_s,x,y):
    # ~ x=_s.find(x); y=_s.find(y)
    # ~ return x==y


# ez volt a vegso
class DynamicConnectivity(object):
  def __init__(_s,N):
    _s.H=[i for i in range(N)]
  def find(_s,x):
    hx=_s.H[x]; hhx=_s.H[hx]
    while hx!=hhx:
      y=_s.H[hhx]
      _s.H[hx]=y
      hx=y; hhx=_s.H[hx]
    _s.H[x]=hhx
    return hhx 
  def union(_s,x,y):
    _s.H[_s.find(x)]=_s.find(y); 
  def connected(_s,x,y):
    return _s.find(x)==_s.find(y)


N=1000000
r1=DynamicConnectivity(N)
for i in range(N-1):
  r1.union(i,i+1)

for a,b in [(0,N-1),(1,N-1)]:
  print(r1.connected(a,b))
