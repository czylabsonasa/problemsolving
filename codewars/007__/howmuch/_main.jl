module How
    export howmuch
    # 37 the first one + k*lkkt(9,7)=k*63, k>=0
    function howmuch(m, n)  
      q,r=divrem(max(0,m-37),63)
      k=37+(q+(r>0))*63
      B=k÷7
      C=k÷9
      ret=[]
      while k<=n
        push!(ret,("M: $k","B: $B","C: $C"))
        k+=63
        B+=9
        C+=7
      end
      ret
    end
end

using .How
println(howmuch(1,100))
println(howmuch(1000,1100))
println(howmuch(3,138))
