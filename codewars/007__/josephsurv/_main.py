def josephus_survivor(n,k):
  nxt=[i+1 for i in range(n)]; nxt[n-1]=0
  pos=n-1
  k-=1
  while n>0:
    for _ in range(k%n): pos=nxt[pos]
    nxt[pos]=nxt[nxt[pos]]
    n-=1
  return nxt[pos]+1

for a,b in [(7,3),(11,19),(1,300),(14,2),(100,1)]:
  print(josephus_survivor(a,b))
  
