def listPosition(word):
  ww=[ord(v)-ord('A') for v in word[::-1]]
  n=len(ww)
  frek=[0]*26
  frek[ww[0]]=1
  A=1
  B=1
  S=0
  for i in range(1,n):
    c=ww[i]
    A*=i
    frek[c]+=1
    B*=frek[c]
    for d in range(c):
      if frek[d]>0: 
        S+=frek[d]*A//B
  return S+1

for w in ['A','ABAB','AAAB','BAAA','QUESTION','BOOKKEEPER']:
  print(listPosition(w))
