def listPosition(word):
  ww=[ord(v)-ord('A') for v in word]
  #print(ww)
  n=len(ww)
  #print(n)
  fakt=[1]*(n+1)
  for i in range(2,n+1): fakt[i]=i*fakt[i-1]
  #print(fakt)
  frek=[0]*26
  for c in ww: frek[c]+=1
  B=1
  for f in frek: B*=fakt[f]
  S=0
  for i in range(n):
    c=ww[i]
    A=fakt[n-i-1]
    for d in range(c):
      if frek[d]>0: 
        S+=(A//(B//frek[d]))
        #print(chr(c+ord('A')),i,chr(ord('A')+d),(A//(B//frek[d])),n-i-1)
    B//=frek[c]
    frek[c]-=1

  return S+1

for w in ['A','ABAB','AAAB','BAAA','QUESTION','BOOKKEEPER']:
  print(listPosition(w))
