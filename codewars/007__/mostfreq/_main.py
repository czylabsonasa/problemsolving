ltr=lambda c: c.isalpha() or c=='\''
def top_3_words(text):
  dd,inw,akt={},False,[]
  for c in text+' ':
    if inw:
      if ltr(c): 
        akt.append(c.lower())
      else:
        akt=''.join(akt)
        if dd.get(akt)==None: dd[akt]=0
        dd[akt]+=1
        akt=[]
        inw=False
    else:
      if ltr(c):
        inw=True
        akt.append(c.lower())
      else:
        pass
  top=[[-1,''],[-1,''],[-1,'']]
  for w in dd.keys():
    fw=[dd[w],w]
    if fw>top[0]: top[0],top[1],top[2]=fw,top[0],top[1]
    elif fw>top[1]: top[1],top[2]=fw,top[1]
    elif fw>top[2]: top[2]=fw
  return [w for nw,w in top if nw>0]

t=[
"""In a village of La Mancha, the name of which I have no desire to call to
mind, there lived not long since one of those gentlemen that keep a lance
in the lance-rack, an old buckler, a lean hack, and a greyhound for
coursing. An olla of rather more beef than mutton, a salad on most
nights, scraps on Saturdays, lentils on Fridays, and a pigeon or so extra
on Sundays, made away with three-quarters of his income.""",
"e e e e DDD ddd DdD: ddd ddd aa aA Aa, bb cc cC e e e",
"  , e   .. ",
"'a' '"
]
for v in t:
  print(top_3_words(v))
