def find_mult_3A(num):
  fr=[0]*10
  for c in str(num): fr[int(c)]+=1
  D=sum(fr)
  tot,last=0,0
  def gen(lev,akt,mod):
    nonlocal D,last,tot
    if 0==mod:
      tot+=1
      last=akt
    if lev==D:return
    for d in range(10):
      if 0==fr[d]:continue
      fr[d]-=1
      gen(lev+1,10*akt+d,(mod+d)%3)
      fr[d]+=1
  
  if num==0: return [1,0]
  
  for d in range(1,10):
    if 0==fr[d]:continue
    fr[d]-=1
    gen(1,d,d%3)
    fr[d]+=1
  
  return [tot,last] # tot=0 ???


#########################################################


def find_mult_3B(num): # elbonyolitottam ?
  if num==0: return [1,0]

  frek=[0]*10
  for c in str(num): frek[int(c)]+=1
  D=sum(frek[1:])
  fakt=[1]*(D+1+frek[0])
  for i in range(2,D+1+frek[0]): fakt[i]=i*fakt[i-1]
  akt=[0]*10
  f0=frek[0]
  
  tot,mx=0,-1

  def comp(lev,n):
    nonlocal mx,tot,f0
    nn=n*10**f0
    if nn>mx: mx=nn
    numer=fakt[lev]
    denom=1
    for i in range(1,10): denom*=fakt[akt[i]]
    tmp=numer//denom
    if f0>0: tmp=fakt[lev+f0]//(denom*(fakt[f0]))-tmp
    tot+=tmp
    

  def gen(d,lev,n,mod):
    nonlocal D
    if 0==mod: comp(lev,n)
    if lev==D: return
    while d>0:
      if frek[d]>0: 
        akt[d]+=1
        frek[d]-=1
        if frek[d]>0:
          gen(d,lev+1,n*10+d,(mod+d)%3)
        if d>1:
          gen(d-1,lev+1,n*10+d,(mod+d)%3)
        frek[d]+=1
        akt[d]-=1      
      d-=1
      
  for d in range(9,0,-1):
    if frek[d]==0: continue
    frek[d]-=1
    akt[d]+=1
    gen(d,1,d,d%3)
    akt[d]-=1
    frek[d]+=1
  
  if f0>0: tot+=1
  return [tot,mx] # tot=0 ??? -> mx=? eg: 11




###########################################################


def find_mult_3C(num): # elbonyolitottam ?
  if num==0: return [1,0]

  frek=[0]*10
  for c in str(num): frek[int(c)]+=1
  f0=frek[0]
  pf0=10**f0
  D=sum(frek[1:])
  fakt=[1]*(D+1+f0)
  for i in range(2,D+1+f0): fakt[i]=i*fakt[i-1]
  most=[0]*10
  avail=[(d,frek[d]) for d in range(1,10) if frek[d]>0]
  
  #print(avail)
  
  tot,mx=0,-1
  def comp(n):
    nonlocal mx,tot,pf0,f0
    nd=sum(most)
    if nd==0: return
    #print(n," ",end="")
    n=n*pf0
    if n>mx: mx=n
    numer=fakt[nd]
    denom=1
    for i in range(1,10): denom*=fakt[most[i]]
    tot+=fakt[nd+f0]//(denom*(fakt[f0]))
    

  def gen(lev,n,mod,volt):
    #print(lev,n,mod)
    if volt and 0==mod: comp(n)
    lev-=1
    if lev<0: return
    d,fd=avail[lev]
    for a in range(fd+1):
      gen(lev,n,mod,a>0)
      most[d]+=1
      n=10*n+d
      mod=(mod+d)%3
    most[d]=0
    
  gen(len(avail),0,0,False)
  #if f0>0: tot+=1; mx=max(mx,0)
  return [tot,mx] # tot=0 ??? -> mx=? eg: 11


#for num in [362,6063,7766553322]:
for num in [6063,11111112323423400011000]:
  print(find_mult_3C(num))
 
  
