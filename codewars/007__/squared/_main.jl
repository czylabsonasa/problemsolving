module Recre
    export list_squared

    function f(n)
      ret=1
      d=2
      while d*d<n
        (n%d==0)&&(ret+=(d^2+(n÷d)^2))
        d+=1
      end
      (d^2==n)&&(ret+=d^2)
      (n>1)&&(ret+=n^2)
      (Int(floor(sqrt(ret)))^2==ret)&&(return ret)
      -1
    end
    # return a Tuple{Int64,Int64}[.....]
    function list_squared(m, n)
      [(k,sk) for (k,sk) in zip(m:n,f.(m:n)) if sk>0]
    end
end

using .Recre
println(list_squared(1,250))
println(list_squared(250,500))
println(list_squared(300,600))


