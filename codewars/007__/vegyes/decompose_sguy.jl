module Sq
    export decompose

    function decompose_aux(n, rem)
        start_n = min(n, Int(floor(sqrt(rem))))
        for i in (start_n:-1:1)
            new_rem = rem - i^2
            if new_rem == 0
                return [i]
            elseif new_rem < 0
                return nothing
            else    
                ans = decompose_aux(i-1, new_rem)
                if ans !== nothing
                    append!(ans, [i])
                    return ans
                end
            end
        end
        return nothing
    end

    function decompose(n)
        return decompose_aux(n-1, n^2)
    end


end



using .Sq

for n in [50,5,2,7,1,77,777,7777,77777,777777,888888888]
  show(decompose(n))
  println()
end

