function int32toip(n)
  n,D=divrem(n,256)
  n,C=divrem(n,256)
  n,B=divrem(n,256)
  n,A=divrem(n,256)
  join(string.([A,B,C,D]),'.')
end

for n in [2149583361,32,0]
  println(int32toip(n))
end
