# Removing Digits
n=int(input())
dist=[0]*(n+1)
queue=[0]*n
queue[0]=n
head,tail=0,1
while head<tail:
  h=queue[head]
  head+=1
  if 0==h: break
  d=dist[h]+1
  for r in '{}'.format(h):
    r=ord(r)-48
    if r>0 and dist[h-r]==0: 
      dist[h-r]=d
      queue[tail]=h-r
      tail+=1
print(dist[0])
