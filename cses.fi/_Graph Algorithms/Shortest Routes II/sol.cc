#include <bits/stdc++.h>
using namespace std;

#pragma GCC optimize("Ofast")
#pragma GCC target("fma,sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,avx2,tune=native")
#pragma GCC optimize("unroll-loops")

using LLI_=long long int;
LLI_ const INF=LLI_(1000000000000);
using P_=pair<LLI_,int>;

#define V_(T_) vector<T_>
#define x_ first
#define y_ second
#define pb_ push_back
LLI_ D[512*512];
#define gc getchar_unlocked
#define pc putchar_unlocked

int getint() {
  int res=0;
  int c;
  while((c=gc()) && !(c>='0'&&c<='9'));
  do {
    res=res*10+c-'0';
  }while ((c=gc()) && (c>='0'&&c<='9'));
  return res;
}

template <typename T> inline void print(const T &n) {
  T N = n;
  if (n < 0) {
    pc('-');
    N = -N;
  }
  T rev = N, count = 0;
  if (N == 0) {
    pc('0');
    return;
  }
  while ((rev % 10) == 0) {
    ++count;
    rev /= 10;
  }
  rev = 0;
  while (N != 0) {
    rev = (rev << 3) + (rev << 1) + N % 10;
    N /= 10;
  }
  while (rev != 0) {
    pc(rev % 10 + '0');
    rev /= 10;
  }
  while (count)
    pc('0'), --count;
}

int main(){
  int N,E,nq; 
  N=getint();
  E=getint();
  nq=getint();
  //V_(V_(LLI_)) D(N+1);
  memset(D, 0x0F, sizeof D);
  for(int a=1;a<=N;a++){
    //D[a].resize(N+1,INF);
    D[(a<<9)+a]=0;
  }

  for(int i=0;i<E;i++){
    int a,b,w;
    a=getint();
    b=getint();
    w=getint();
    if(w<D[(a<<9)+b]){
      D[(a<<9)+b]=D[(b<<9)+a]=w;
    }
  }

  
  for(int k=1;k<=N;k++){
    for(int a=1;a<=N;a++){
      //LLI_ dak=D[a][k];
      for(int b=1;b<=N;b++){
        D[(a<<9)+b]=min(D[(k<<9)+b]+D[(a<<9)+k],D[(a<<9)+b]);
      }
    }
  }

  for(int q=0;q<nq;q++){
    int a,b;
    a=getint();
    b=getint();
    LLI_ d=D[(a<<9)+b];
    if(d<INF){
      print(d);
      puts("");
    }else puts("-1");
    //printf("%lld\n",d<INF ? d : -1);
  }

  return 0; 
}
