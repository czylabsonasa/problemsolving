#include <bits/stdc++.h>
using namespace std;
using LLI_=long long int;
using P_=pair<LLI_,int>;

#define V_(T_) vector<T_>
#define x_ first
#define y_ second
#define pb_ push_back

int main(){
  int N,E,nq; scanf("%d%d%d",&N,&E,&nq);
  V_(V_(P_)) G(N+1);
  for(int i=0;i<E;i++){
    int a,b,w;scanf("%d%d%d",&a,&b,&w);
    G[a].pb_(P_({w,b}));
    G[b].pb_(P_({w,a}));
  }

  V_(V_(LLI_)) D(N+1);
int uj=0;
  for(int q=0;q<nq;q++){
    int a,b;scanf("%d%d",&a,&b);
    if(D[a].size()>0){
      printf("%lld\n",D[a][b]);
      continue;
    }
    if(D[b].size()>0){
      printf("%lld\n",D[b][a]);
      continue;
    }
fprintf(stderr,"%d\n",++uj);
    auto& Da=D[a];
    Da.resize(N+1,-1);
    priority_queue<P_, V_(P_), greater<P_>> H; // minHeap
    H.push(P_({0,a}));
    Da[a]=0;
    while(H.size()>0){    
      auto t=H.top(); H.pop();
      //LLI_ d=t.x_;int u=t.y_;
      int u=t.y_; LLI_ d=Da[u];

      for(auto s:G[u]){
        LLI_ w=s.x_;int v=s.y_;
        if(Da[v]<0 || d+w<Da[v]){
          Da[v]=d+w;
          H.push(P_({d+w,v}));
        }
      }
    }
    printf("%lld\n",Da[b]);
  }

  return 0; 
}
