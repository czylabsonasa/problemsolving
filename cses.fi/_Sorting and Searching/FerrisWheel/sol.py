# ferris wheel, sort,greedy
n,x=[int(v) for v in input().split()]
p=sorted(int(v) for v in input().split())
i,j=0,n-1
g=0
while i<j:
  if p[i]+p[j]<=x: 
    i+=1
  g+=1
  j-=1
g+=(i==j)
print(g)