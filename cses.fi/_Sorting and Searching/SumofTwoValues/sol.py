def o(): return [int(v) for v in input().split()]
n,x=o()
a=sorted(zip(o(),range(1,n+1)))
i,j=0,n-1
while i<j:
  u,v=a[i],a[j]
  if u[0]+v[0]>x:
    j-=1
  elif u[0]+v[0]<x:
    i+=1
  else:
    i,j= (u[1],v[1]) if u[1]<v[1] else (v[1],u[1])
    break
if i>=j:
  print('IMPOSSIBLE')
else:
  print(i,j)


