# variaciok osszege nem olyan egyszeru minta a kombinaciokra (2^n)
M=10**9+7
i2=500000004

import sys;input=sys.stdin.readline
N=int(input())
mx=0
x=[0]*N
for i in range(N):
  x[i]=int(input())
  mx=max(mx,x[i])

ap=[0]*(mx+1)
ap[2]=1
for n in range(2,mx):
  ap[n+1]=(n*ap[n]+n)%M

for n in x:
  print(((n*ap[n]-n*(n-1))*i2)%M)