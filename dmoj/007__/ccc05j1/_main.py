d=int(input())
e=int(input())
w=int(input())

A=0
if d>100: A=A+25*(d-100)
A+=e*15
A+=w*20

B=0
if d>250: B=B+45*(d-250)
B+=e*35
B+=w*25

print("Plan A costs {0:d}.{1:02d}".format(A//100,A%100))
print("Plan B costs {0:d}.{1:02d}".format(B//100,B%100))
while True:
  if A<B:
    print("Plan A is cheapest.")
    break
  if A>B:
    print("Plan B is cheapest.")
    break
  print("Plan A and B are the same price.")
  break
  
  