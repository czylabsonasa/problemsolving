# bfs, queue
steps=[(1,2),(1,-2),(-1,2),(-1,-2),(2,1),(2,-1),(-2,1),(-2,-1)]

sx,sy=map(int,input().split())
tx,ty=map(int,input().split())
INF=1000
dist=[[INF]*9 for _ in range(9)]
qq=[(0,0)]*8*8
dist[sx][sy]=0
qq[0]=(sx,sy)
head,tail=0,1
while head<tail:
  sx,sy=qq[head]; head+=1
  if sx==tx and sy==ty: break
  d=dist[sx][sy]+1
  for st in steps:
    x=sx+st[0]; y=sy+st[1]
    if x<1 or x>8 or y<1 or y>8 or dist[x][y]<INF: continue
    dist[x][y]=d
    qq[tail]=(x,y); tail+=1

print(dist[tx][ty])     
