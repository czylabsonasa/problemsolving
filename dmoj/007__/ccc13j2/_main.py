s=set(input().strip())
if s.issubset({'I','O','S','H','Z','X','N'}):
  print("YES")
else:
  print("NO")
