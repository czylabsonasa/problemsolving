word=input().strip().lower()
n=len(word)
pre=[[-1]*n for _ in range(n)]
for i in range(n): 
  for j in range(i+1): pre[i][j]=1

mx=1
def pali(i,j):
  global mx,word,pre
#  if pre[i][j]>=0 or mx>=(j-i+1): return # premature opt., fails on "bggbxafa"
  if pre[i][j]>=0: return
  if word[i]==word[j]:
    pali(i+1,j-1)
    if 1==pre[i+1][j-1]:
      pre[i][j]=1
      mx=max(mx,j-i+1)
    else:
      pre[i][j]=0
  else:
    pre[i][j]=0
  pali(i+1,j)
  pali(i,j-1)

pali(0,n-1)
print(mx)
