word=input().strip()
n=len(word)
pre=[[-1]*n for _ in range(n)]
for i in range(n): 
  for j in range(i+1): pre[i][j]=1

mx=1
def pali(i,j):
  global mx,word,pre
  if pre[i][j]>=0 or mx>(j-i+1): return
  if word[i]==word[j]:
    pali(i+1,j-1)
    if 1==pre[i+1][j-1]:
      pre[i][j]=1
      mx=max(mx,j-i+1)
      return
  else:
    pre[i][j]=0
  pali(i+1,j)
  if 1==pre[i+1][j]:
    mx=max(mx,j-i)
    return
  pali(i,j-1)
  if 1==pre[i][j-1]:
    mx=max(mx,j-i)

pali(0,n-1)
print(mx)
