def nextPerm(x):
  n=len(x)
  i=n-1
  while i>0 and x[i-1]>=x[i]: i-=1
  if 0==i: return (x,False)
  j=i
  while j<n and x[j]>x[i-1]: j+=1
  x[i-1],x[j-1]=x[j-1],x[i-1]
  y=x[i:]; y.reverse(); x[i:]=y
  return (x,True)

s=[int(c) for c in input().strip()]
s,nxt=nextPerm(s)
if False==nxt: 
  print("0")
else:
  print("".join([str(c) for c in s]))  
  
  