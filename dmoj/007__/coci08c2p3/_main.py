# positive bi and so, nem szamit
N=int(input())
bi,so=[],[]
for _ in range(N):
  s,b=map(int,input().split())
  bi.append(b); so.append(s)
opt=abs(bi[0]-so[0])
for i in range(1,N):
  opt=min(opt,abs(bi[i]-so[i]))
bi.append(-1); so.append(-1)  

def gen(level,b,s):
  global opt
  bb=bi[level]; ss=so[level]  
  if bb<0:
    opt=min(opt,abs(b-s))
    return
  gen(level+1,b+bb,s*ss)
  gen(level+1,b,s)
    
for i in range(N):
  gen(i+1, bi[i],so[i])
print(opt)  