# https://dmoj.ca/problem/ccc07s3
dd={}
gg=[]
n=0 # the nodes will be numbered 0...n-1
for _ in range(int(input())):
  a,b=map(int,input().split())
  if dd.get(a)==None:
    dd[a]=n; n+=1; 
    gg.append(-1)
  if dd.get(b)==None:
    dd[b]=n; n+=1; 
    gg.append(-1)
  a=dd[a]
  b=dd[b]
  gg[a]=b

vis=[0]*n
col=[0]*n
sta=[0]*n
c2siz=[0]*n

c=0
for a in range(n):
  if col[a]!=0: continue
  aa=a
  c+=1
  v=0
  while True:
    sta[v]=aa; vis[aa]=v; v+=1; col[aa]=c
    aa=gg[aa]
    if col[aa]!=0: 
      if col[aa]==c:
        c2siz[c]=v-vis[aa]
      break

  v=vis[aa]-1
  while v>=0:
    aa=sta[v]; col[aa]=-1
    v-=1
  
while True:
  a,b=map(int,input().split())
  if 0==a and 0==b: break
  a=dd[a]; b=dd[b]
  if col[a]<0 or col[b]<0 or col[a]!=col[b]:
    print("No")
  else:
    a=vis[a]; b=vis[b];
    if a<b:
      print("Yes {0:d}".format(b-a-1))
    else:
      print("Yes {0:d}".format(c2siz[col[a]]-(a-b)-1))
      
