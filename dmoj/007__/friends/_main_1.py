# https://dmoj.ca/problem/ccc07s3
# kicsit atvarialtam de hibas lett...
N=10000
gg=[0]*N
n=0 # the nodes will be numbered 0...n-1
for _ in range(int(input())):
  a,b=map(int,input().split())
  gg[a]=b

vis=[0]*N
col=[0]*N
sta=[0]*N
c2siz=[0]*N

c=0
for a in range(1,N):
  if gg[a]==0: continue
  if col[a]!=0: continue
  aa=a
  c+=1
  v=0
  while True:
    sta[v]=aa; vis[aa]=v; v+=1; col[aa]=c
    aa=gg[aa]
    if col[aa]!=0: 
      if col[aa]==c:
        c2siz[c]=v-vis[aa]
      break

  v=vis[aa]-1
  while v>=0:
    aa=sta[v]; col[aa]=-1
    v-=1
  
while True:
  a,b=map(int,input().split())
  if 0==a and 0==b: break
  if col[a]<0 or col[b]<0 or col[a]!=col[b]:
    print("No")
  else:
    a=vis[a]; b=vis[b];
    if a<b:
      print("Yes {0:d}".format(b-a-1))
    else:
      print("Yes {0:d}".format(c2siz[col[a]]-(a-b)-1))
      
