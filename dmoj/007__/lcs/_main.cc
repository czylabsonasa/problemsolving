// lcs

#include <bits/stdc++.h>
using namespace std;
#define __getchar getchar_unlocked
int getnum(){
  int c;
  while((c=__getchar())<'0' || c>'9');
  int x=c-'0';
  while((c=__getchar())>='0' && c<='9'){
    x=10*x+c-'0';
  }
  return x;
}

int main(){
  int na=1+getnum(); int nb=1+getnum();
  vector<int> dd(1001,0);
  vector<int> a(na,0);
  for(int i=1;i<na;i++){
    int v=getnum();
    a[i]=v;
    dd[v]=1;
  }
  vector<int> b(nb,0);
  int j=0;
  for(int i=1;i<nb;i++){
    int v=getnum();
    if(dd[v]==1){
      b[++j]=v;
    }
  }
  nb=j+1;
      
  vector<int> x(nb,0),px(nb,0);
  for(int i=1;i<na;i++){
    swap(x,px);
    int ai=a[i];
    for(int j=1;j<nb;j++){
      if(ai==b[j]){x[j]=1+px[j-1];}
      else{x[j]=max(x[j-1],px[j]);}
    }
  }
  printf("%d\n",x[nb-1]);

  return 0;
}
