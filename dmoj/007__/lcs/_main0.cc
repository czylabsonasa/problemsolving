// lcs

#include <bits/stdc++.h>
using namespace std;
#define __getchar getchar_unlocked
int getnum(){
  int c;
  while((c=__getchar())<'0' || c>'9');
  int x=c-'0';
  while((c=__getchar())>='0' && c<='9'){
    x=10*x+c-'0';
  }
  return x;
}

int main(){
  int na=getnum(); int nb=getnum();
  vector<int> dd(1001,0);
  vector<int> pa(na);
  for(int i=0;i<na;i++){
    pa[i]=getnum();
    dd[pa[i]]=1;
  }
  vector<int> pb(nb);
  for(int i=0;i<nb;i++){
    pb[i]=getnum();
    if(dd[pb[i]]==1){
      dd[pb[i]]=2;
    }
  }
  vector<int> a={0};
  for(auto v:pa){if(dd[v]==2){a.push_back(v);}}
  vector<int> b={0};
  for(auto v:pb){if(dd[v]==2){b.push_back(v);}}
  na=a.size();  
  nb=b.size();  
      
  vector<int> x(nb,0),px(nb,0);
  for(int i=1;i<na;i++){
    swap(x,px);
    int ai=a[i];
    for(int j=1;j<nb;j++){
      if(ai==b[j]){x[j]=1+px[j-1];}
      else{x[j]=max(x[j-1],px[j]);}
    }
  }
  printf("%d\n",x[nb-1]);

  return 0;
}
