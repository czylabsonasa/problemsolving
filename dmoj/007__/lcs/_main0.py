# lcs
na,nb=map(int,input().split())
a=[-1]+list(map(int,input().split()))
b=[-2]+list(map(int,input().split()))
x=[0]*(nb+1)
px=[0]*(nb+1)
for i in range(1,na+1):
  ai=a[i]
  x,px=px,x
  for j in range(1,nb+1):
    if ai==b[j]: x[j]=1+px[j-1]
    else: x[j]=max(x[j-1],px[j])

print(x[nb])
