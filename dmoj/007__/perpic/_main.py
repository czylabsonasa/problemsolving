from math import sqrt,floor
while True:
  n=int(input())
  if 0==n: break
  k=int(floor(sqrt(n)))
  while n%k>0: k-=1
  m=n//k
  print("Minimum perimeter is {0:d} with dimensions {1:d} x {2:d}".format(2*(k+m),k,m))
