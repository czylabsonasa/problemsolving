def pfact(x):
  if 1==x: return []
  f=[]
  while 0==x%2:
    f.append(2)
    x//=2 # ?       
  while 0==x%3:
    f.append(3)
    x//=3 # ?       
  d=5; delta=2  
  while d*d<=x:
    while 0==x%d:
      f.append(d)
      x//=d # ?       
    d+=delta; delta=6-delta
  if x>1: f.append(x)
  return f

n=int(input())
for _ in range(n):
  f=pfact(int(input()))
  print(" ".join(list(map(str,f))))  
   
  