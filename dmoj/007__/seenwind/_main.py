# a valtozasi gyorsasaga masodfokuval irhato le
H=int(input())
M=int(input())

pol=[-6,H,2,1,0]
def P(x):
  px=0
  for v in pol:
    px=px*x+v
  return px

t=1
while t<=M and P(t)>0: t+=1

if t>M:
  print("The balloon does not touch ground in the given time.")
else:
  print("The balloon first touches ground at hour:")
  print(t)

