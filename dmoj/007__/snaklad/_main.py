tab=list(range(101))
spec=[(99,77),(90,48),(54,19),(67,86),(40,64),(9,34)]
for a,b in spec:
  tab[a]=b
akt=1
while True:
  t=input().strip()
  if len(t)==0:
    print("You Quit!")
    break
  t=int(t)
  if 0==t: 
    print("You Quit!")
    break
  if akt+t<=100:
    akt=tab[akt+t]
  print("You are now on square {0:d}".format(akt))
  if akt==100:
    print("You Win!")
    break
