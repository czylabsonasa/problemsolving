n=int(input())
van=int(input())
ok=[0]*(n+1)
req=['S']*(n+1)
for i in range(1,n+1):
  req[i]=input()
for i in range(van):
  tip,num=input().split()
  num=int(num)
  if tip>=req[num]: ok[num]=1 # using the spec: 'S'>'M'>'L'
print(sum(ok))  
