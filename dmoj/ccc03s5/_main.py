#ccc03s5
from sys import stdin; input=stdin.readline

from heapq import heapify, heappop, heappush # MIN
nv,ne,ntar=map(int,input().split())
gg=[[] for _ in range(nv+1)]
for _ in range(ne):
  a,b,w=map(int,input().split())
  gg[a].append((w,b))
  gg[b].append((w,a))

tar=[0]*(ntar)
for i in range(ntar):
  tar[i]=int(input())

dd=[-1]*(nv+1)
hh=[(-2*10**9,1)]
while len(hh)>0:
  d,a=heappop(hh)
  d=-d
  for w,b in gg[a]:
    t=min(d,w)
    if t>dd[b]:
      dd[b]=t
      heappush(hh,(-t,b))

print(min(dd[v] for v in tar))