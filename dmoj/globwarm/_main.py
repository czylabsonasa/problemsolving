import sys; input=sys.stdin.readline


while True:
  ntemp,*temp=map(int,input().split())
  if ntemp==0: break
  #print(temp)
  opt=ntemp-1
  if ntemp>2:
    for i in range(ntemp-1):
      temp[i]=temp[i+1]-temp[i]
    temp=temp[:-1] # length: ntemp-1 
#    print(temp)
    for lp in range(1,ntemp-1): # candidate for length of period: lp
      x=temp[:lp]
      tlp=lp
      while tlp+lp<=ntemp-2 and x==temp[tlp:tlp+lp]:
        tlp+=lp
      if tlp+lp>ntemp-2: 
        if tlp>ntemp-2: 
          opt=lp
          break
        marad=ntemp-1-tlp
        if x[:marad]==temp[tlp:tlp+marad]:
          opt=lp
          break

  print(opt)
