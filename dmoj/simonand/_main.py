import sys; input=sys.stdin.readline
# dp, coin

c,maxW=map(int,input().split())

V=[0];W=[0] # val,weight
for _ in range(c):
  v,w=map(int,input().split())
  if w>maxW or v==0:continue
  V.append(v); W.append(w)

ans=sum(V)
nV=len(V)-1
if nV>0 and sum(W)>maxW:
  dp=[0]*(maxW+1)
  for j in range(1,nV+1):
    v,w=V[j],W[j]
    #dp[w]=max(v,dp[w]) # ide nem szabad mert felhasznalja ezt
    for i in range(maxW-w,0,-1):
      if dp[i]==0: continue
      dp[i+w]=max(dp[i]+v,dp[i+w])
    dp[w]=max(v,dp[w]) # ide kell
  
  ans=max(dp)

print(ans)
