import sys
(cages, cap) = (int(n) for n in sys.stdin.readline().split())
dp = [0 for n in range(cap+1)]

for n in range(cages):
    (prin, songs) = (int(i) for i in sys.stdin.readline().split())
    for i in range(cap,songs-1,-1):
        dp[i] = max(dp[i-songs] + prin,dp[i])
print dp[-1]
