#include <bits/stdc++.h>
using namespace std;

int main(){
  long long int N=600851475143;
  long p=2;
  typedef pair<int,int> IP;
  vector<IP> lista;
  while(N>1){
    int k=0;
    while(N%p==0){
      k+=1;
      N/=p;
    }
    if(k>0){
      lista.push_back(IP({p,k}));
    }
    p+=1;
  }
  auto utolso=lista[lista.size()-1];
  printf("%d %d\n",utolso.first, utolso.second);    

  return 0;
}
