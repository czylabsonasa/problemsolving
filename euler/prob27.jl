let 
  function pSieve(N)
    sieve=fill(Int8(1),N)
    sieve[4:2:N] .= 0
    sieve[1]=0
    p=3
    while p*p<=N
      if 1==sieve[p]
        sieve[p*p:2p:N] .= 0
      end
      p+=2
    end
    sieve
  end


  isp=pSieve(2200000)
  # isp[1:20] |> println

  opt=(len=0,a=0,b=0)
  for a in -1000:1000, b in -1000:1000
    f(x)=x^2+a*x+b
    x=0
    while true
      fx=f(x)
      (fx<=0 || 0==isp[fx])&&break
      x+=1
    end
    if x>opt.len
      opt=(len=x,a=a,b=b)
    end
  end

  opt |> println
end