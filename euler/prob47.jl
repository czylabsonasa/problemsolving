let 
  function pSieve(N) # modified for distinct factors
    sieve=fill(1,N)
    sieve[4:2:N] .= 2
    sieve[1]=0
    p=3
    while p*p<=N
      if 1==sieve[p]
        sieve[p*p:2p:N] .= p
      end
      p+=2
    end
    for p in 4:N
      d=sieve[p]
      (1==d)&&continue
      pp=p
      while 0==pp%d
        pp=pp÷d
      end
      sieve[p]=sieve[pp]+1
    end
    sieve
  end

  N=10^6 # ad hoc limit
  isp=pSieve(N)


  #findall(x->x==4, isp) |> println
  TAR=4
  mind=[]
  i0=-1
  for i in 1:N
    if isp[i]==TAR
      if i0==-1
        i0=i 
      end
    else
      if i0>0
        if i-i0>TAR-1
          push!(mind,(i0,i-1))
        end
        i0=-1
      end
    end
  end
  mind |> println

end
