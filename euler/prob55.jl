#prob55
#reverse and add

INF=50

function steps(a::BigInt)
  da=digits(a) # reversed
  s=1
  while true
    (s==INF)&&break
    a=a+parse(BigInt,da|>join)
#println(" ",a)
    da=digits(a)
    (da == reverse(da))&&break
    s+=1
  end
  s
end

let
  nL=0
  for x in 1:10000
#println(x, " ",steps(big(x)))
    if steps(big(x))==INF
      nL+=1
    end
  end
  nL |> println
end