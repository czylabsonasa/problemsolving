# prob63

# x_{n-1}10^{n-1}+...+x_{1}10+x_{0} = y^n
# 0<y<10
# 10^{n-1}>9^{n} -> n>log(10)/(log(10)-log(9)) \approx 21.85 -> n<22

let
  s=9 # 1 digit num: 1st power of itself
  for y in 2:9
    for n in 2:21
      z=big(y)^n
      if digits(z)|>length == n
        s+=1
        println(y," ",n," ",z)
      end
    end
  end
  s |> println
end