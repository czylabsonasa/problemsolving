#prob83

using DelimitedFiles

A=[
  0 1 1 1 1 1;
  0 0 0 0 1 1;
  1 1 1 0 1 1;
  1 0 0 0 1 1;
  1 0 1 1 1 1;
  1 0 0 0 0 0
]

A=[
  131 673 234 103 18;
  201 96 342 965 150;
  630 803 746 422 111;
  537 699 497 121 956;
  805 732 524 37 331
]

let
  A=readdlm("p083_matrix.txt",',',Int,'\n') # 20210531 the same as of 081
  r,c=size(A)

  #INF=typemax(Int)
  INF=2*sum(A)
  outq=fill(true,r,c)
  M=fill(INF,r,c)
  M[1,1]=A[1,1]
  q=fill((0,0),10*r*c)
  q[1]=(1,1)
  outq[1,1]=false

  update(i,j,ii,jj)=
    if 1≤ii≤r && 1≤jj≤c
      if M[i,j]+A[ii,jj]< M[ii,jj]
        M[ii,jj]=M[i,j]+A[ii,jj]
        true
      else
        false
      end
    else
      false
    end

  # sok felesleges vizsgalat...
  
  head=1
  tail=2
  while head<tail
    i,j=q[head]
    head+=1
    outq[i,j]=true

    #(i==r && j==c)&&break
    if true==update(i,j,i-1,j)
      if true==outq[i-1,j]
        outq[i-1,j]=false
        q[tail]=(i-1,j)
        tail+=1
      end
    end
    if true==update(i,j,i+1,j)
      if true==outq[i+1,j]
        outq[i+1,j]=false
        q[tail]=(i+1,j)
        tail+=1
      end
    end
    if true==update(i,j,i,j-1)
      if true==outq[i,j-1]
        outq[i,j-1]=false
        q[tail]=(i,j-1)
        tail+=1
      end
    end
    if true==update(i,j,i,j+1)
      if true==outq[i,j+1]
        outq[i,j+1]=false
        q[tail]=(i,j+1)
        tail+=1
      end
    end
  end

  #M|>println

  M[r,c] |> println
end