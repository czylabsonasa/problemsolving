for _ in range(int(input())):
  sd=set(input().strip()).symmetric_difference(input().strip())
  print(''.join(c for c in 'abcdefghijklmnopqrstuvwxyz' if c in sd))
