L=int(input())
n=int(input())
for _ in range(n):
    w,h=map(int,input().split())
    if min(w,h)<L:
        print("UPLOAD ANOTHER"); continue
    if max(w,h)>L:
        print("CROP IT"); continue
    print("ACCEPTED")
