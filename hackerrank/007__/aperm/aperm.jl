let
	Q=parse(Int,readline())
	for _ in 1:Q
		n,k=parse.(Int,split(readline()))
		path,ava=zeros(Int,n),trues(n)
		fnd=false
		function sear(akt)
			akt>n && (fnd=true;return)
#			akt-k>0 && ava[akt-k] && (path[akt]=akt-k;ava[akt-k]=false;sear(akt+1);ava[akt-k]=true)
			if akt-k>0 && ava[akt-k]
				path[akt]=akt-k;ava[akt-k]=false;sear(akt+1);ava[akt-k]=true
			end

			(true==fnd) && return
#			akt+k<=n && ava[akt+k] && (path[akt]=akt+k;ava[akt+k]=false;sear(akt+1)ava[akt+k]=true)
			if akt+k<=n && ava[akt+k]
				path[akt]=akt+k;ava[akt+k]=false;sear(akt+1);ava[akt+k]=true
			end
		end
		sear(1)
		if false==fnd
			println("-1")
		else
			println(join(path,' '))
		end
	end
end