#include <bits/stdc++.h>
using namespace std;

struct Info{
	string s;
	int h;
	bool operator<(const Info&masik)const{
		if(h>masik.h){return false;}
		if(h<masik.h){return true;}
		return s<masik.s;
	}
};

int main(){
	int n;cin>>n;
	vector<Info> arr(n);
	for(int i=0;i<n;i++){
		cin>>arr[i].s;
		arr[i].h=arr[i].s.length();
	}
	sort(arr.begin(),arr.end());
	for(int i=0;i<n;i++){
		cout<<arr[i].s<<"\n";
	}
	
	return 0;
}