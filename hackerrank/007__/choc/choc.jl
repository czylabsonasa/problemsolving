let
  Q=parse(Int,readline())
  for _ in 1:Q
    n,c,m=parse.(Int,split(readline()))
    ans=div(n,c)
    w=ans
    while w>=m 
      q,r=divrem(w,m)
      ans+=q
      w=q+r
    end
    println(ans)
  end  
end
