#include <bits/stdc++.h>
using namespace std;

int main(){
  int n; scanf("%d",&n);
  vector<int> data(n+1);
  int D=0;
  for(int i=1;i<=n;i++){
    scanf("%d",&data[i]);
    D+=data[i];
  }
  vector<vector<int>> G(n+1);
  vector<int> deg(n+1,0);
  for(int i=1;i<n;i++){
    int a,b;scanf("%d%d",&a,&b);
    G[a].push_back(b); ++deg[a];
    G[b].push_back(a); ++deg[b];
  }
  vector<int> Q(n+1);
  int head=0,tail=0;
  for(int i=1;i<=n;i++){
    if(1==deg[i]){
      Q[tail++]=i;
    }
  }
  int mn=D+111;
  while(head!=tail){
    int a=Q[head];head++;
    if(deg[a]<1){
      break;
    }
    deg[a]=0;
    mn=min(mn,abs(D-2*data[a]));
    for(int b:G[a]){
      if(deg[b]>0){
        data[b]+=data[a];
        if(--deg[b]==1){
          Q[tail++]=b;
        }
      }
    }
  }
  printf("%d\n",mn);
  
  return 0; 
}
