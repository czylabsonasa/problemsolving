# friend circle
def ins(x,d,H,sH):
  if d.get(x)==None:
    n=d[-1]; d[-1]+=1
    d[x]=n
    H.append(n)
    sH.append(1)
  return d[x]

def find(x,H):
  if H[x]!=x:
    H[x]=find(H[x],H)
  return H[x] 

def unio(x,y,H,sH):
  if x!=y:
    x=find(x,H)
    y=find(y,H)
    H[x]=y
    sH[y]+=sH[x]
  return sH[y]
  

dd={-1:0}
HH=[]
sHH=[]
mx=0
nr=int(input())
for _ in range(nr):
  a,b=map(int,input().split())
  a=find(ins(a,dd,HH,sHH),HH)
  b=find(ins(b,dd,HH,sHH),HH)
  mx=max(mx,unio(a,b,HH,sHH))
  print(mx)

