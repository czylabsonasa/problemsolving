#include <bits/stdc++.h>
using namespace std;
int main(){
  int const B=512;
  char line[B];
  int m,n;scanf("%d%d",&m,&n);
  vector<bitset<B>> table(m);
  for(int i=0;i<m;i++){
    scanf("%s",line);
    table[i]=bitset<B>(string(line));
  }
  int mx=-1, nmx=0;
  for(int i=0; i<m-1;i++){
    for(int j=i+1;j<m;j++){
      int tmp=(table[i] | table[j]).count();
      if(tmp<mx){continue;}
      if(tmp==mx){
        nmx+=1;
        continue;
      }
      mx=tmp; nmx=1;
    }
  }
  printf("%d\n%d\n",mx,nmx);

  return 0; 
}
