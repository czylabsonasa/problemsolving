#include <bits/stdc++.h>
using namespace std;

int main(){
	int frek[256];
	int Q;cin>>Q;
	while(Q--){
		int n;string s;cin>>n>>s;
		auto solve=[&frek,s](){
			for(int i='A';i<='Z';i++){frek[i]=0;}frek['_']=0;
			for(auto v:s){
				++frek[v];
			}
			for(int i='A';i<='Z';i++){
				if(1==frek[i]){return false;}
			}
			if(0==frek['_']){
				int state='_';
				for(auto v:s){
					if(state!=v){
						if(frek[state]!=0){return false;}
						state=v;
					}
					--frek[state];
				}
			}
			return true;
		};


		cout<<(solve()?"YES\n":"NO\n");
	}

	return 0;
}