let 
  n,q=parse.(Int,split(readline()))
  p1,p2=zeros(Int,n+1),zeros(Int,n+1) # location of prev 1,2
  w=parse.(Int,split(readline()))
  t1,t2=-1,-1
  for i in 1:n
    p1[i],p2[i]=t1,t2
    (w[i]==1)&&(t1=i;continue)
    (w[i]==2)&&(t2=i;continue)
  end
  p1[n+1],p2[n+1]=t1,t2
  for _ in 1:q
    a,b=parse.(Int,split(readline()))
    a,b=a+1,b+1
    ans=3
    if p2[b+1]>=a ans=2 end
    if p1[b+1]>=a ans=1 end
    println(ans)
  end
  
end