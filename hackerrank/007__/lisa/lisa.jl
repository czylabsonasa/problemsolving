let
  n,k=parse.(Int,split(readline()))
  arr=parse.(Int,split(readline()))
  ans,pg=0,1
  for ch in arr 
    ex=1 
    while ch>0
      d=min(ch,k)
      (ex<=pg<ex+d) && (ans=ans+1)
      ch-=d; 
      ex+=d
      pg+=1
    end
  end
  println(ans)
end
