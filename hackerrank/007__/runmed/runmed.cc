#include <bits/stdc++.h>
using namespace std;

int main(){
  priority_queue<int,vector<int>,less<int>> L; // maxHeap
  priority_queue<int,vector<int>,greater<int>> R; // minHeap
  //~ R.push(1);
  //~ R.push(2);
  //~ printf("%d\n",R.top());
  //~ return 0;

  int n; scanf("%d",&n);
  for(int i=0;i<n;i++){
    int t;scanf("%d",&t);
    L.push(t);
    while(1){
      t=L.top();
      if(L.size()==R.size()+2){
          L.pop();
          R.push(t);
          break;
      }
      if(0==R.size()){
        break;
      }
      int tt=R.top();
      if(t>tt){
        L.pop(); R.pop();
        L.push(tt); R.push(t);
      }
      break;
    }//w(1)
    
    if(L.size()>R.size()){
      printf("%.1lf\n",double(L.top()));
    }else{
      printf("%.1lf\n",0.5*(double(L.top())+R.top()));
    }
  }
    
  return 0; 
}
