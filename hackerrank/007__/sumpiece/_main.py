M=10**9+7
n=int(input())
arr=[0]+list(map(int,input().split()))
dd=[None]*(n+1)
dd[1]=(arr[1],1)
  
def gen(b):
  if dd[b]!=None: return
  osszes=0; db=0
  vege=arr[b]
  for m in range(b-1,0,-1):
    gen(m)
    eleje=dd[m]
    osszes=((osszes+eleje[0])+vege*eleje[1]*(b-m))%M
    vege+=arr[m]
    db+=eleje[1]
  dd[b]=((osszes+vege*b)%M,db+1)
gen(n)
print(dd[n][0])
