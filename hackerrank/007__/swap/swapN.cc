#include <bits/stdc++.h>
using namespace std;

struct node{
  int L;
  int R;
  int dep;
  node():L(-1),R(-1),dep(-1)
  {}
};
node tree[1111];

function<void(int,int)> funA=[](int s,int d){
  int t;
  t=tree[s].L;
  if(-1!=t){
    tree[t].dep=d;
  }
  t=tree[s].R;
  if(-1!=t){
    tree[t].dep=d;
  }
};
function<void(int)> funB=[](int s){};

void travIn(int s){
  if(-1==s){return ;}
  funA(s,tree[s].dep+1);
  travIn(tree[s].L);
  funB(s);
  travIn(tree[s].R);
}

int main(){
  int n; cin>>n;
  for(int s=1;s<=n;s++){
    cin>>tree[s].L>>tree[s].R;
  }
  tree[1].dep=1;
  travIn(1);
  funA=[](int s,int d){return;}; //nullázva
  funB=[](int s){cout<<s<<" ";};

  int t; cin>>t;
  for(int i=0;i<t;i++){
    int k;cin>>k;
    for(int j=1;j<=n;j++){
      if(0==tree[j].dep%k){
        swap(tree[j].L,tree[j].R);
      }
    }
    travIn(1);
    cout<<"\n";
  }

  return 0; 
}
