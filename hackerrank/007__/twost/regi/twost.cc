#include <bits/stdc++.h>
using namespace std;


int main(){
  typedef long long int LLI;
  int ng;scanf("%d",&ng);
  while(ng--){
    int nA,nB,K;scanf("%d%d%d",&nA,&nB,&K);
    vector<int> A(nA+1),B(nB+1);
    LLI s=0; // LLI -> avoid overflow: sumAll can be 10^11
    for(int i=0;i<nA;++i){
      scanf("%d",&A[i]);  
      s+=A[i];
    }
    for(int i=0;i<nB;++i){
      scanf("%d",&B[i]);  
      s+=B[i];
    }
    int res=nA+nB;
    if(s>K){
      A[nA]=B[nB]=K+999;//sentinel
      res=0; // !
      s=0; 
      int benn=0, state=0;//0: A feltoltes 1: B feltoltes 2: visszaszedes A-rol,B feltoltes, 3:finish
      int iA=0,iB=0;
      while(state<3){
        while(0==state){
          if(iA>=nA||s+A[iA]>K){
            state=1;
            res=max(benn,res);
            break;
          }
          s+=A[iA]; iA+=1; benn+=1;                  
        }
        while(1==state){
          if(s+B[iB]>K){
            state=2;
            res=max(benn,res);
            break;
          }
          s+=B[iB]; iB+=1; benn+=1;   
        }
        while(2==state){
          if(0==iA){
            state=3;
            break;
          }
          iA-=1;s-=A[iA];benn-=1;
          while(iB<nB && s+B[iB]<=K){
            s+=B[iB]; iB+=1; benn+=1;
          }
          res=max(res,benn);
          if(iB>=nB){
            state=3; 
            break;
          }
        }
      }// outermost while
    }// if sumAll>K
    
    printf("%d\n",res);
  }// game descriptions
  return 0;
}
