let
  x=[1 2 4]'
  A=[0 1 0; 0 0 1; 1 1 1]
  M=10^9+7
  function pow(n)
    if n>1
      B=(pow(n÷2)^2).%M
      (n%2>0)&&(B=(B*A).%M)
      B
    else
      #A[:,:]
      A
    end
  end

  readline()
  a=sort(parse.(Int,split(readline())))
  s,p,pp=0,-1,-1
  for v=a
    (v==p)&&(s+=pp;continue)
    p=v
    pp= if p>3 (pow(p-1)*x)[1]%M else x[p] end
    s+=pp
  end  
  println(s%M)

end