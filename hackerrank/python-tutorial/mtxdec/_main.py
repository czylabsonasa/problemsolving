import re
r,c=map(int,input().split())
#print(r,c)
msg=['' for _ in range(c)]
#print(msg)

for _ in range(r):
  ll=input()
  for j in range(c):
    #print(j,len(msg))
    msg[j]+=ll[j]
#msg=''.join(msg)
#print(msg)
print(re.sub(r'(\w)\W+(?=\w)',r'\1 ',''.join(msg)))
