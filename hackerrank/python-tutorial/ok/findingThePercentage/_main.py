n = int(input())
di = {}
for _ in range(n):
  name, *line = input().split()
  sco = list(map(float, line))
  di[name] = sum(sco)/len(sco)
print("{0:.2f}".format(di[input()]))
