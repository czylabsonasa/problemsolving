# Enter your code here. Read input from STDIN. Print output to STDOUT

import re
r1=r'[456]\d{3}(-\d{4}){3}$'
r2=r'[456]\d{3}(\d{4}){3}$'


for _ in range(int(input())):
  v=''
  while True:
    v=input().strip()
    if len(v)!=0:break
  out='Invalid'
  if re.match(r1,v)!=None or re.match(r2,v)!=None:
    out='Valid'
    p,f=-1,-1
    for c in v+'-':
      if not c.isdigit(): continue
      if not c==p:
        if f>3:
          out='Invalid'; break
        p=c; f=0
      f+=1
  print(out)
