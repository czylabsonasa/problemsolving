let
  akt,prev=fill(0,4,4),fill(0,4,4)
  olv(h=2)=h>1 ? parse.(Int,split(readline())) : parse(Int,readline())
  ini(x,y)=(for i=1:4 x[i,:].=y[i]; x[i,i]=-1; end)

  for _ in 1:olv(1)
    d=olv(1)
    ini(akt,olv())
    for i=2:d
      prev,akt=akt,prev
      ini(akt,olv())
      for j=1:4,k=1:4
        (k!=j)&&(akt[j,k]+=max(prev[k,1:4 .!=j]...))
      end
    end
    println(max(akt...))
  end
end