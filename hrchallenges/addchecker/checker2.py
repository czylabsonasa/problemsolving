def run_custom_checker(t_obj, r_obj):
    # Don't print anything to STDOUT in this function
    # Enter your custom checker scoring logic here

    r_obj.result = True
    r_obj.score = 1.0
    r_obj.message = "Success"

    try:
        result_data = open(t_obj.testcase_output_path, 'r').read().split()
    except IOError:
        r_obj.result = False
        r_obj.score = 0
        r_obj.message = 'Error reading result file'
        return
    if len(result_data)<3:
        r_obj.result = False
        r_obj.score = 0
        r_obj.message = 'Unsufficient data'
        return
    if tip(result_data[0])[0]!=int or tip(result_data[1])[0]!=float or tip(result_data[2])[0]!=str:
        r_obj.result = False
        r_obj.score = 0
        r_obj.message = 'Wrong answer'
        return 
    return    
        

# End of BODY
        