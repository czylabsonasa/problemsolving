using LinearAlgebra, Polynomials, StatsBase
mround(x,d=4)=round(x,digits=d)

Q=30
#println(fin,Q)
for q=2:40
  fin=open("input/input$(q).txt","w")
  d=rand(2:10)
  tol=1/10^rand(6:11)
  a,b=sample(-5:5,2,replace=false,ordered=true)
  p=rand(-5:5,d+1)
  println(fin,d," ",a," ",b," ",tol)
  println(fin,join([string(v) for v in p], " "))
  

  p=Poly(p[end:-1:1])
  
  fout=open("output/output$(q).txt","w")
  pa,pb=polyval(p,[a,b])
  if pa*pb>0
    println(fout,"Inf")
  else
    while b-a>tol
      m=0.5*(a+b)
      pm=polyval(p,m)
      if pm*pb<=0
        a=m
        pa=pm
      else
        b=m
        pb=pm
      end
    end
    println(fout,0.5*(a+b))
  end
  close(fin)
  close(fout)
end

