név:
matrixszorzat

rövid leírás:
mátrixok szorzása


leírás:
Adott két mátrix esetén számold ki a szorzatukat, ha lehet.

input:
két mátrix leírást tartalmaz. egy mátrix leírás 
két egész számmal indít, mely a sorok és oszlopok száma, 
majd a mátrix elemei következnek sorfolytonosan, azaz 
$r$ sor, mindegyikben $c$-szám.


korlátok:
1<r,c<100

output:
Ha nem végezhető el a szorzás, írj ki $-1$-et. Egyébként, 
ha $C=(c_{ij})$ a szorzatmátrix, írd ki az 
|\sum_{ij} c_{ij}|$ összeget, $9$ jegyre kerekítve.

