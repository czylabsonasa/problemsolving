név:
polreg1

rövid leírás:
polinomiális regresszió



leírás:
Adott egy $d$ egész szám és $n$ pont a síkon. Határozd meg a pontokat legkisebb 
négyzetesen legjobban közelítő (legfeljebb) $d$-fokú $p$ polinomot. Ha ez egyértelmű, akkor 
írd ki a $p(A),p(B)$ számokat, ahol $A=min(t)-1$ és $B=max(t)+1$, ahol 
$t$ a pontok első koordinátáinak vektora. Ha több megoldás van, akkor 
egyszerűen írj ki -1-et. <br>
Fontos: az egyszerűség kedvéért tekintsük úgy, hogy a megoldás nem egyértelmű, ha 
$cond_1(A)>10^6$ teljesül. 


input:
$d$<br>
$n$<br>
$t_1 \ldots t_n$<br>
$f_1 \ldots f_n$<br>



korlátok:
$0<d<10$<br>
$0<n<30$


output:
A leírásnak megfelelő szám(ok), soronként egy érték, 9 jegyre kerekítve.<br> 
Lásd a minta-kimeneteket.
