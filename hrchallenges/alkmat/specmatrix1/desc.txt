név:
specmatrix1

rövid leírás:
spéci mátrix létrehozása 1


leírás:
Adott $n$ egész szám esetén hozd létre azt az $n\times n$-es $A=(a_{ij})$ 
mátrixot, melynek diagonálisa és anti-diagonálisa az $[1,2,\ldots,n]$ vektort 
tartalmazza, a mátrix többi eleme $0$. <br>
Pontosabban, melyre $a_{i,i}=a_{i,n+1-i}=i,\ \ i=1,\ldots,n$ és 
$a_{i,j}=0 \ \text{ ha } i\neq j \text{ és } i+j \neq n+1$.

input:
Egy szám: $n$.


korlátok:
$1<n<50$

output:
A leírásnak megfelelő mátrix. Lásd a minta-kimeneteket.
