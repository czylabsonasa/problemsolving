using LinearAlgebra
mround(x,d=4)=round(x,digits=d)

Q=30
#println(fin,Q)
for n=3:Q
  fin=open("input/input$(n).txt","w")
  println(fin,n)
  A=diagm(1:n)
  A=A+A[1:end,n:-1:1]
  if n%2==1
    m=1+n÷2
    A[m,m]=m
  end


  sA=join([ join(string.(A[i,1:end])," ") for i in 1:n], "\n" )
  fout=open("output/output$(n).txt","w")
  println(fout,sA)

  close(fin)
  close(fout)
end

