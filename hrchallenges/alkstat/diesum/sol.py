N=50
dp=[[0]*(6*N+1) for i in range(N+1)]
for i in range(1,7):
  dp[1][i]=1/6.0

for n in range(2,N+1):
  for s in range(n,n*6+1):
    P=0.0
    for k in range(1,7):
      if s-k<n-1: 
        break  
      P+=dp[n-1][s-k]
    dp[n][s]=P/6.0

n=int(input())
dat=["{0:.12f}".format(dp[n][k]) for k in range(n,6*n+1)]
print("\n".join(dat))


