# N tétel van K-t tud. 2-t húz visszatevés nélkül
# sikeres vizsga valsége,
# nem huzza mindkettőt a N-K közül, 
# várható osztályzat kerekítve.

solve(N,K)=(p=binomial(N-K,2)/binomial(N,2);p+(1.0-p)*(2+3+4+5)/4.0)



for fn=0:0
  fin=open("input/input$(fn).txt","w")

  n=rand(10:30)
  k=rand(n-1:n-1)
  println(fin,n," ",k)


  r=solve(n,k)
  r=round(r,digits=0)

  fout=open("output/output$(fn).txt","w")
  println(fout,r)

  close(fin)
  close(fout)
end
