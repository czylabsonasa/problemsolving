# N tétel van K-t tud. 2-t húz visszatevés nélkül
# sikeres vizsga valsége,
# nem huzza mindkettőt a N-K közül

solve(N,K)=1.0-binomial(N-K,2)/binomial(N,2)


n,k=parse.(Int,split(readline()))
println(solve(n,k))

