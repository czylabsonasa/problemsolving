# N-es lottoban mennyi az esely hogy nincs ket szomszedos szam

function pozfelbont(A,B)# A-t B-db pozitiv szamra bontjuk fel
  binomial(A-1,B-1)
end

function solve(N,K)
  (2*pozfelbont(N-K,K)+pozfelbont(N-K,K-1)+pozfelbont(N-K,K+1))/binomial(N,K)
end


for fn=0:2
  fin=open("input/input$(fn).txt","w")
  K=rand(3:4)
  N=rand(10K:10K+3)
  println(fin,N," ",K)

  fout=open("output/output$(fn).txt","w")
  println(fout,1.0-solve(N,K))

  close(fin)
  close(fout)
end

