# N-es lottoban mennyi az esely hogy nincs ket szomszedos szam
function pozfelbont(A,B)# A-t B-db pozitiv szamra bontjuk fel
  binomial(A-1,B-1)
end

function solve(N,K)
  (2*pozfelbont(N-K,K)+pozfelbont(N-K,K-1)+pozfelbont(N-K,K+1))/binomial(N,K)
end


n,k=parse.(Int,split(readline()))
println(1.0-solve(n,k))

