function f(x)
  n=length(x)
  r=fill(1.0,n+1)
  for i=1:n
    r[i+1]=(1-x[i])*r[i]
    r[i]=x[i]*r[i]
  end
  r
end

p=parse.(Float64,split(readline()))
p=f(p)
p[1:end-1]./=(1.0-p[end])
println(join(p[1:end-1],"\n"))