1;
p=fscanf(stdin,"%f ");
%display(sum(p));
function r=f(x)
  n=length(x);
  r=ones(1,n+1);
  for i=1:n
    r(i+1)=(1-x(i))*r(i);
    r(i)=x(i)*r(i);
  end
end
fp=f(p);
oszt=1.0-fp(end);
fprintf(stdout,"%.15f\n",fp(1:end-1)/oszt);