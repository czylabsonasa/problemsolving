using StatsBase

Q=5
#println(fin,Q)
for n=1:Q
  fin=open("input/input$(n).txt","w")
  # X,Y=sample(1:1000,2,replace=false,ordered=true)
  # a=rand(2:11)
  # d=rand(5:1000)
  X,Y=sample(1:10,2,replace=false,ordered=true)
  a=rand([2,10])
  d=rand(5:6)

  println(fin,X," ",Y," ",a," ",d)

  sol=fill(0,d)
  for it=1:d
    sol[it]=X÷Y
    X=a*(X%Y)
  end
  sol="0."*join(string.(sol[2:end]))


  fout=open("output/output$(n).txt","w")
  println(fout,sol)

  close(fin)
  close(fout)
end

