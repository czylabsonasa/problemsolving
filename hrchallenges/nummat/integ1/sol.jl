mround(x,d=4)=round(x,digits=d)
msimp(x,d=6)=floor(x*10.0^d)/10.0^d
function horner(p)
  function val(x)
    px=0
    for v=p
      px=px*x+v
    end
    px
  end
  val
end

p=parse.(Float64,split(readline()))
a,b=parse.(Float64,split(readline()))
pol=horner(p)
m=0.5*(a+b)
mp=(b-a)*pol(m)
trap=(b-a)*0.5*(pol(a)+pol(b))
simp=(b-a)/6*(pol(a)+4*pol(m)+pol(b))
dat=mround.([mp,trap,simp])
println(join(dat,"\n"))


