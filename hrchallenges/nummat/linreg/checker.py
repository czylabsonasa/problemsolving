# x is a string
def f2s(path):
  with open(path, 'r') as f:
    data = f.read()
  return data
  
def tip(x):
  try:
    y=int(x)
    return int,y
  except:
    pass

  try:
    y=float(x)
    return float,y
  except:
    pass

  return str,x



def check(ok,cand):# two list of strings
  if len(ok)!=len(cand):
    return False,0.0,"length error"

  for o,c in zip(ok,cand):
    to,tc=tip(o),tip(c)
    while True:
      if to[0]!=tc[0]:
        return False,0.0,"type error"
      if to[0]==float:
        if abs(to[1]-tc[1])>1e-4:
          return False,0.0,"float error"
        break     
      if to[0]==int:
        if to[1]!=tc[1]:
          return False,0.0,"integer error"
        break
      if to[1]!=tc[1]:
        return False,0.0,"string error"
      break
  return True,1.0,"ok"


def run_custom_checker(t_obj, r_obj):
  def setr(r,s,m):
    r_obj.result,r_obj.score,r_obj.message=r,s,m

  try:
    result_data = f2s(t_obj.testcase_output_path)
    exp_data = f2s(t_obj.testcase_expected_output_path)
  except IOError:
    setr(False,0.0,"Error reading result file")
    return

  setr(*check(exp_data.split(),result_data.split()))
  return    
