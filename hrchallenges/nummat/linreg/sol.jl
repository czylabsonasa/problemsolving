t=parse.(Float64,split(readline()))
f=parse.(Float64,split(readline()))
x=parse.(Float64,split(readline()))
n=length(t)
s=n*sum(t.*t)-sum(t)^2
if s<1e-12
  println("ambigous")
else
  b1=(n*sum(t.*f)-sum(t)*sum(f))/s
  b0=(sum(f)-b1*sum(t))/n
  L(x)=b1*x+b0
  println(join(L.(x)," "))
end
