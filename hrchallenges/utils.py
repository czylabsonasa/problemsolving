def mreadmtx(r,c):
  return [[float(v) for v in input().split()] for i in range(r)]
def mprod(A,B):
  rA,rB=len(A),len(B)
  cA,cB=len(A[0]),len(B[0])
  if cA!=rB: return None
  C=[[0]*cB for i in range(rA)]
  for i in range(rA):
    for j in range(cB):
      s=0.0
      for k in range(cA):
        s+=A[i][k]*B[k][j]
      C[i][j]=s
  return C
def msum(A):
  return sum([sum(v) for v in A])  
def mabs(A):
  return [[abs(v) for v in rows] for rows in A]
def mcol(A,j):
  return [row[j] for row in A]
