function makef(s)
  next()=(s=(113*s+51)%77;s)
  next
end
f=makef(10)

function fillandsum(A,L,f)
  for i in 1:L 
    @simd for j in 1:L 
      @inbounds A[i][j]=f() 
    end
  end
  sum(sum(A))
end



function solve(S,N)
  A=[fill(0,maximum(S)) for _ in 1:maximum(S)]

  ret=0
  for L ∈ S
    #A=[fill(0,L) for _ in 1:L]
    for _ in 1:N
      ret+=fillandsum(A,L,f)
    end
  end
  ret
end
