function robotPath(s,h)
a,b=0,0
for c in s
  (c=='L')&&(b>-h)&&(b-=1)
  (c=='R')&&(b<h)&&(b+=1)
  (c=='D')&&(b<h)&&(a+=1)
  (b>-h)&&(a-=1)
end
[a,b]
end
println(robotPath("LLLLUUUUDR",2))
