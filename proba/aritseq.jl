function lss(a)
  s=Set()
  d=Dict()
  for v in a
    for w in s
      d[v,v-w]=get(d,(w,v-w),1)+1
    end
    push!(s,v)
  end
  maximum(values(d))
end