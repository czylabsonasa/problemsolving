import sys; input=sys.stdin.readline

def fun(data):
  window_size = 10                                                                          
  windows = [data[i:i+window_size] for i in range(0, len(data)-window_size)]        
  return sum(1 for x in windows if 't' in x)

print(fun(input().strip()))
