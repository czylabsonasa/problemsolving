
# 4: 9
# 5:16
# 6:20
# 7:30(?)
n=7
H=(n-1)*n-1
A=set([0])
B=set()
sAB=set()

maximin=1000000000

def genB(lev,sb,sa):
  #print('B',lev,sb,sa)
  global sAB,H
  i=sb-1
  while i<H:
    i+=1
    uj=set(i+a for a in A)
    if uj & sAB: continue
    sAB=sAB|uj
    B.add(i)
    genA(lev+1,sa,i+1)
    B.remove(i)
    sAB=sAB-uj

def genA(lev,sa,sb):
  #print('A',lev,sa,sb)
  global sAB,H,maximin
  if lev==2*n:
    H=max(sa,sb)-1
    sA=sorted(A)
    sB=sorted(B)
    maximin=min(maximin,max(sA[-1],sB[-1]))
    print(sA,sB)
    return 
  i=sa-1
  while i<H:
    i+=1
    uj=set(i+b for b in B)
    if uj & sAB: continue
    sAB=sAB|uj
    A.add(i)
    genB(lev+1,sb,i+1)
    A.remove(i)
    sAB=sAB-uj
genB(1,1,1)
print(maximin)