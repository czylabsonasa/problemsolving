#=
def mkCum(arr):
  parr=list(arr)
  narr=len(arr)
  for i in range(1,narr):
    parr[i]+=parr[i-1]
  def cum(i,j):
    i=max(i,0)
    j=min(j,narr-1)
    if 0==i:
      return parr[j]
    else:
      return parr[j]-parr[i-1]
  return cum
=#

def SegTree(arr, f, ini):
  narr=len(arr)
  tree=[]*4*narr
  ans=0
  def init(a=0,b=narr-1,idx=1):
    if a==b:
      tree[idx]=arr[a]
      return
    m=(a+b)//2
    init(a,m,2*idx)
    init(m+1,b,2*idx+1)
    tree[idx]=f(tree[2*idx],tree[2*idx+1])
  init(0,narr-1,1)
  def qq(A,B,a,b,idx):
    if B<a or A>b: return
    if A==a and B==b: ans=f(ans,tree[idx]); return
    m=(a+b)//2
    qq(A,min(B,m),a,m,2*idx)
    qq(A,max(B,m+1),m+1,b,2*idx+1)
  def query(A,B):
    ans=ini
    qq(A,B,0,narr-1,1)
    return ans
  return query
