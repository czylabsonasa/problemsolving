from math import sqrt
from decimal import Decimal

def Baby(A,dig):
  prec=10**(-dig-1)
  x=A
  while True:
    nx=(x+A/x)/2
    if abs(x-nx)<prec: break
    x=nx
  return x



def Format(x,dig):
  sx='{:.18f}'.format(float(x))
  ip,fp=sx.split('.')
  if dig:
    return ip+'.'+fp[:dig]
  return ip


# ~ for x,dig in [
# ~ (10,2),(101,4),(8765,8),(3000,3),(9999999,11),(10**9+1,12),(10**10,0),
# ~ (123456789,10),(123456789123,10),(123456789123123123123,10),(777777777,11),
# ~ (555666777,10)]:
  # ~ f=lambda y:Format(y,dig)
  # ~ print(f(Baby(x,dig)))
  # ~ print(f(sqrt(x)))
  # ~ print(f(Decimal(x).sqrt()))

from random import randint

while True:
  x=randint(10**2,10**9)
  dig=randint(0,15)
  f=lambda y:Format(y,dig)
  b=f(Baby(x,dig))
  s=f(sqrt(x))
  d=f(Decimal(x).sqrt())
  if d!=s or h!=s: 
    print(x,dig)
    break

