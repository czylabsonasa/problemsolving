def SegTree(arr, f, ini):
  narr=len(arr)
  print(narr)
  tree=[0]*4*narr
  print("tree:",len(tree))
  ans=0
  def init(A,B,idx):
    if A==B:
      tree[idx]=arr[A]
      return
    M=(A+B)//2
    init(A,M,2*idx)
    init(M+1,B,2*idx+1)
    tree[idx]=f(tree[2*idx],tree[2*idx+1])
  init(0,narr-1,1)
  def qq(a,b,A,B,idx):
    print("[",a,",",b,"]","[",A,",",B,"]")
      if a>b or b<A or a>B: return
    nonlocal ans
    if A==a and B==b: ans=f(ans,tree[idx]); return
    M=(A+B)//2
    qq(a,min(b,M),A,M,2*idx)
    qq(max(a,M+1),b,M+1,B,2*idx+1)
  def query(A,B):
    nonlocal ans
    ans=ini
    qq(A,B,0,narr-1,1)
    return ans
  return query

q=SegTree([i for i in range(1000)],lambda a,b:max(a,b),0)
q(10,100)