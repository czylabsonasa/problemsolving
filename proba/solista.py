mylist = ["dog", "a", "b", "-b", "cat", "-cat", "-a", "-dog"]
# mylist = ["dog", "a", "b", "-a", "-b", "-dog"]

result=[]
for v in mylist:
  if len(result)>0 and v=='-'+result[-1]:
    result.pop()
  else:
    result.append(v)

print(result)
