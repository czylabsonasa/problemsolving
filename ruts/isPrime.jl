function isp(x)
  ret=true
  while true
    (1==x) && (ret=false; break)
    (x<4) && break
    (0==x%2 || 0==x%3)&&(ret=false; break)
    L=Int(floor(sqrt(x)))
    d=5
    delta=2
    while d<=L
      (0==x%d) && (ret=false; break)
      d+=delta
      delta=6-delta
    end
    break
  end
  ret
end

# for i in 1:30
#   println(i," ",isp(i))
# end

# sum of digits + divisibility by 3(9) rule
# 1 2 3  4  5  6  7  8  9
# 1 3 6 10 15 21 28 36 45
# _     __       __

using Combinatorics
mind=[]
for pp in permutations("1234")
  p=parse(Int, join(pp))
  if isp(p)
    push!(mind,p)
  end
end
for pp in permutations("1234567")
  p=parse(Int, join(pp))
  if isp(p)
    push!(mind,p)
  end
end

# sort(mind) |> println
sort!(mind)
mind[end] |> println
