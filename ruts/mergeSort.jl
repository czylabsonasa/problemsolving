let 
	function Merge(T,n)
		arr=Array{T}(undef,n)
		tmp=Array{T}(undef,n)
		invsz=0
		function _sort(a,b)
			invsz=0
			sort(a,b)
			invsz
		end
		function sort(a,b) 
			a>=b&&return
			m=div(a+b,2)
			sort(a,m)
			sort(m+1,b)
			merge(a,m,b)
		end
		function merge(a,m,b)
			l,r,t=a,m+1,a
			while true
				(l>m||r>b)&&break
				if arr[r]<arr[l]
					tmp[t]=arr[r]; t+=1; r+=1
					invsz+=(m-l+1)
				else
					tmp[t]=arr[l]; t+=1; l+=1
				end
			end
			while l<=m
				tmp[t]=arr[l]; t+=1; l+=1
			end
			while r<=b
				tmp[t]=arr[r]; t+=1; r+=1
			end
			arr[a:b]=tmp[a:b] # copy back
		end
		arr,_sort
	end

	arr,srt=Merge(Int,100000+8)
	T=parse(Int,readline())
	for _ in 1:T
		n=parse(Int,readline())
		arr[1:n]=parse.(Int,split(readline()))
		println(srt(1,n))
	end
end
