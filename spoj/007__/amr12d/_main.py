import sys;input=sys.stdin.readline
t=int(input())
for _ in range(t):
  s=input().strip()
  # ~ print('s=',s,s[::-1])
  ans='NO'
  rs=s[::-1]
  if s==rs:
    n=len(s)
    s1=set();s2=set()
    for i in range(n):
      for j in range(i+2,n):
        # ~ print(s[i:j], rs[i:j])
        s1=s1|set([s[i:j]])
        s2=s2|set([rs[i:j]])
    # ~ print(s1)
    # ~ print(s2)
    if s1==s2: ans='YES'
  print(ans)
