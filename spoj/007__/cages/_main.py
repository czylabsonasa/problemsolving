import sys;input=sys.stdin.readline
n=int(input())
perm=[int(input())-1 for _ in range(n)]

ans=['_']*n

C=[[0,'A'],[0,'B'],[0,'C']]

     
i=0
while i<n:
  if ans[i]=='_': 
  # cycle
    C.sort();c=0
    ai,pi=i,-1
    while True:
      ans[ai]=C[c][1]
      C[c][0]+=1
      c= c+1 if c<2 else 0
      ni=perm[ai]
      if ni==i: 
        if ans[ai]==ans[i]: # 3k+1 len cycle (no fixpoint)
          ans[pi],ans[ai]=ans[ai],ans[pi]
        break
      pi=ai # prev
      ai=ni
  i+=1

print(''.join(ans))
