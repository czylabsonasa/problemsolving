T=int(input())
t=0
while t<T:
  t+=1
  n=int(input())-1
  s=sum(int(v) for v in input().split())
  print("Case {0:d}: {1:d}".format(t,(s*2**n)%100000007))
  
