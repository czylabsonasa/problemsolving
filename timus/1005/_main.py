n=int(input())
arr=list(map(int,input().split()))
S=sum(arr)
opt=S+1
for g in range(2**n): 
  tg=g
  asum=0
  for v in arr:
    if 0==tg: break
    if 1==tg%2: asum+=v
    tg//=2
  opt=min(opt,abs(S-2*asum))

print(opt)